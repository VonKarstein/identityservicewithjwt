﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace IdentityServiceWithJWT.Models.Identity
{
    public class ApplicationUser : IdentityUser
    {
        public DateTimeOffset LastVisit { get; set; }
        /// <summary>
        /// Block user
        /// </summary>
        public bool? IsEnabled { get; set; }
    }
}