﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace IdentityServiceWithJWT.Models.Identity
{
    public class Customer
    {
        [System.ComponentModel.DataAnnotations.Key]
        public Guid Id { get; set; }

        //public string IdentityId { get; set; }

        public ApplicationUser Identity { get; set; }
        public string Location { get; set; }
        public string Locale { get; set; }
        public string Gender { get; set; }       
    }
}
