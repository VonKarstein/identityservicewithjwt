﻿using IdentityServiceWithJWT.Models.Token;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace IdentityServiceWithJWT.Models.ApplicationInformation
{
    public class Application
    {
        [System.ComponentModel.DataAnnotations.Key]
        public Guid Id { get; set; }

        [StringLength(256)]
        public string Name { get; set; }
        
        //public virtual ApplcationType ApplcationType { get; set; }

        //public virtual ICollection<RefreshToken> RefreshTokens { get; set; }
    }
}
