﻿using IdentityServiceWithJWT.Models.Token;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace IdentityServiceWithJWT.Models.ApplicationInformation
{
    public class ApplicationType
    {
        [System.ComponentModel.DataAnnotations.Key]
        public Guid Id { get; set; }

        [StringLength(256)]
        public string Name { get; set; }

        [StringLength(256)]
        public string ClientSecret { get; set; }

        [StringLength(256)]
        public string ClientId { get; set; }

        //[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        //public ICollection<Application> Applications { get; set; }

        public virtual ICollection<RefreshToken> RefreshTokens { get; set; }
    }
}
