﻿using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IdentityServiceWithJWT.Models.Token
{
    public class TokenOptions
    {
        public const string ISSUER = "AuthenticateJWT"; // издатель токена
        public const string AUDIENCE = "YachtsAPI"; // потребитель токена
        public const string KEY = "asjd[saposajdpok;lksdaoqwhie]pasoj";   // ключ для шифрации
        public const int JwtExpireDays = 1; // время жизни токена - 20 минута
        public static SymmetricSecurityKey GetSymmetricSecurityKey()
        {
            return new SymmetricSecurityKey(Encoding.ASCII.GetBytes(KEY));
        }
    }
}
