﻿using IdentityServiceWithJWT.Models.ApplicationInformation;
using IdentityServiceWithJWT.Models.Identity;
using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace IdentityServiceWithJWT.Models.Token
{
    public class RefreshToken
    {
        [System.ComponentModel.DataAnnotations.Key]
        public Guid Guid { set; get; }
        public ApplicationUser Identity { set; get; }  

        public Guid Token { set; get; }

        public bool Expiration { set; get; }

        public ApplicationType ApplicationType { set; get; }

    }
}
