﻿using IdentityServiceWithJWT.Data;
using IdentityServiceWithJWT.Models.Identity;
using Microsoft.AspNetCore.Identity;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace IdentityServiceWithJWT.Models.Token
{
    public class CreateToken
    {
        public async Task<(string, Guid)> GenerateJwtToken(ApplicationUser user, UserManager<ApplicationUser> _usermanager)
        {
            var encodedJwt = await CreateAccessToken(user, _usermanager);
            var refreshToken = await CreateRefreshToken(user);

            return (encodedJwt, refreshToken);
        }


        private async Task<string> CreateAccessToken(ApplicationUser user, UserManager<ApplicationUser> _userManager)
        {
            var userClaims = await _userManager.GetClaimsAsync(user);

            userClaims.Add(new Claim(JwtRegisteredClaimNames.Sub, user.Email));
            userClaims.Add(new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString()));
            userClaims.Add(new Claim(ClaimTypes.NameIdentifier, user.Id.ToString()));
        
            //        var claims = new List<Claim>
            //{
            //    new Claim(JwtRegisteredClaimNames.Sub, user.Email),
            //    new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString()),
            //    new Claim(ClaimTypes.NameIdentifier, user.Id.ToString()),
            //    //new Claim(ClaimsIdentity.DefaultRoleClaimType, user)
            //};

            var key = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(TokenOptions.KEY));
            var creds = new SigningCredentials(key, SecurityAlgorithms.HmacSha256);

            var expires = DateTime.Now.AddMinutes(Convert.ToDouble(TokenOptions.JwtExpireDays));

            var token = new JwtSecurityToken(
                TokenOptions.ISSUER,
                TokenOptions.AUDIENCE,
                userClaims,
                expires: expires,
                signingCredentials: creds
            );

            return new JwtSecurityTokenHandler().WriteToken(token);
        }
#pragma warning disable CS1998 // Async method lacks 'await' operators and will run synchronously
        private async Task<Guid> CreateRefreshToken(ApplicationUser user)
#pragma warning restore CS1998 // Async method lacks 'await' operators and will run synchronously
        {
            //  var refreshToken = Guid.NewGuid().ToString().Replace("-", "");
            var refreshToken = Guid.NewGuid();

            return refreshToken;
        }
    }
}
