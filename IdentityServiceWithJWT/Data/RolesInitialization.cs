﻿using IdentityServiceWithJWT.Models.Identity;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;



namespace IdentityServiceWithJWT.Data
{
    public class RolesInitialization
    {
        public static async Task CreateRoles(IServiceProvider serviceProvider,
            IConfiguration Configuration, ApplicationDbContext app)
        {
            var RoleManager = serviceProvider.GetRequiredService<RoleManager<IdentityRole>>();
            var UserManager = serviceProvider.GetRequiredService<UserManager<ApplicationUser>>();
            string[] roleNames = { "Admin", "Manager", "Member" };
            IdentityResult roleResult;

            foreach (var roleName in roleNames)
            {
                var roleExist = await RoleManager.RoleExistsAsync(roleName);
                if (!roleExist)
                {
                    roleResult = await RoleManager.CreateAsync(new IdentityRole(roleName));
                }
            }

            await CreateSuperUser(Configuration, UserManager);
            await CreateClient(app);
        }

        private static async Task CreateSuperUser(IConfiguration Configuration, UserManager<ApplicationUser> UserManager)
        {
            var poweruser = new ApplicationUser
            {
                UserName = Configuration.GetSection("AppSettings")["UserEmail"],
                Email = Configuration.GetSection("AppSettings")["UserEmail"]
            };
            string userPassword = Configuration.GetSection("AppSettings")["UserPassword"];
            var user = await UserManager.FindByEmailAsync(Configuration.GetSection("AppSettings")["UserEmail"]);

            if (user == null)
            {
                var createPowerUser = await UserManager.CreateAsync(poweruser, userPassword);
                if (createPowerUser.Succeeded)
                {
                    await UserManager.AddToRoleAsync(poweruser, "Admin");
                }
            }
        }
        private static async Task CreateClient(ApplicationDbContext applicationDbContext)
        {
            var item = new Models.ApplicationInformation.ApplicationType
            {
                ClientId = "123456",
                ClientSecret = "1234567890",
                Name = "Xamarin"
            };
            if (applicationDbContext.ApplcationTypes.SingleOrDefault(value => value.Name == item.Name) != null)
            {
                return;
            }
            applicationDbContext.ApplcationTypes.Add(item);
            await applicationDbContext.SaveChangesAsync();

        }

    }
}
