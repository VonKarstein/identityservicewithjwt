﻿using IdentityServiceWithJWT.Models.ApplicationInformation;
using IdentityServiceWithJWT.Models.Identity;
using IdentityServiceWithJWT.Models.Token;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace IdentityServiceWithJWT.Data
{
    public class ApplicationDbContext : IdentityDbContext<ApplicationUser>
    {     
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options)
              : base(options)
        {
        }


        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);
            // Customize the ASP.NET Identity model and override the defaults if needed.
            // For example, you can rename the ASP.NET Identity table names and more.
            // Add your customizations after calling base.OnModelCreating(builder);
        }

        public DbSet<Customer> Customers { get; set; }
        public DbSet<RefreshToken> RefreshTokens { get; set; }
        public DbSet<ApplicationType> ApplcationTypes { get; set; }


       // public DbSet<Application> Applications { get; set; }
    }
}
