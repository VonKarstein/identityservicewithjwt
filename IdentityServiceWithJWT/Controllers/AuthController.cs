﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using IdentityServiceWithJWT.Data;
using IdentityServiceWithJWT.Extensions;
using IdentityServiceWithJWT.Models.Token;
using IdentityServiceWithJWT.Services;
using IdentityServiceWithJWT.ViewModels;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;

using Microsoft.EntityFrameworkCore;
using IdentityServiceWithJWT.Models.ApplicationInformation;
using IdentityServiceWithJWT.Models.Identity;
using System.Security.Claims;
using System.Text.Encodings.Web;

namespace IdentityServiceWithJWT.Controllers
{
    [AllowAnonymous]
    [Produces("application/json")]
    [Route("[controller]")]
    [ApiController]
    public class AuthController : Controller
    {
        private readonly SignInManager<ApplicationUser> _signInManager;
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly IConfiguration _configuration;

        private readonly IEmailSender _emailSender;

        public AuthController(
            ApplicationDbContext applicationDbContext,
            UserManager<ApplicationUser> userManager,
            SignInManager<ApplicationUser> signInManager,
            IEmailSender emailSender,
            IConfiguration configuration
            )
        {
            _userManager = userManager;
            _signInManager = signInManager;
            _configuration = configuration;
            _emailSender = emailSender;
            _applicationDbContext = applicationDbContext;
        }
        ApplicationDbContext _applicationDbContext;



        [HttpPost("Registration")]
        public async Task<object> Register([FromBody] RegistrationViewModel model)
        {
            var user = new ApplicationUser
            {
                UserName = model.Email,
                Email = model.Email,
            };
            var result = await _userManager.CreateAsync(user, model.Password);

            if (result.Succeeded)
            {
                var claim = new Claim("Users", "Member");
                await _userManager.AddClaimAsync(user, claim);
                await _userManager.AddToRoleAsync(user, "Member");

                var code = await _userManager.GenerateEmailConfirmationTokenAsync(user);

                var callbackUrl = Url.EmailConfirmationLink(user.Id.ToString(), code, Request.Scheme);

                // await _emailSender.SendEmailConfirmationAsync(model.Email, callbackUrl);

                var emailStatus = await _emailSender.SendEmailAsync(model.Email, "Confirm your email",
                 $"Please confirm your account by clicking this link: <a href='{HtmlEncoder.Default.Encode(callbackUrl)}'>link</a>");

                if (emailStatus)
                    return Ok("Confirm your email");
                else
                    return Ok("Account is registered");
            }
            return BadRequest();
        }

        [AllowAnonymous]
        [HttpPost("Login")]
        public async Task<object> Login([FromBody] LoginViewModel model) //Login with password
        {
            var appUser = _userManager
                .Users
                .SingleOrDefault(r => r.Email == model.Email);

            if (appUser is null)
                return BadRequest("User does not exist");
            if (appUser.IsEnabled.GetValueOrDefault())
            {
                return BadRequest("User is locked. Please reset to password.");
            }

            if (!appUser.EmailConfirmed)
                return BadRequest("It is necessary to confirm the registration. Instructions sent to the mail.");

            var result = await _signInManager
                .PasswordSignInAsync(appUser, model.Password, false, true);

            if (result.IsLockedOut)
            {
                var dateOfLockEnd = await _userManager.GetLockoutEndDateAsync(appUser);
                if (dateOfLockEnd.HasValue)
                {
                    var x = TimeZoneInfo.ConvertTimeFromUtc(dateOfLockEnd.Value.UtcDateTime, TimeZoneInfo.Local);

                    return BadRequest($"Password was entered 3 times incorrect. The lock will be finished at {x}");
                }
            }
            if (result.Succeeded)
            {
                appUser.LastVisit = DateTimeOffset.Now;
                _applicationDbContext.Update(appUser);
                await _applicationDbContext.SaveChangesAsync();

                var item = _applicationDbContext.RefreshTokens
                    .Include(value => value.Identity)
                    .Include(value => value.ApplicationType)
                    .Where(value => value.Identity == appUser
                    && value.Expiration == false
                    && value.ApplicationType.ClientId == model.ClientId
                    && value.ApplicationType.ClientSecret == model.ClientSecret)
                    .SingleOrDefault();
                if (item != null)
                {
                    item.Expiration = true;
                    _applicationDbContext.Update(item);
                    await _applicationDbContext.SaveChangesAsync();

                    return await GenereteToken(appUser, item.ApplicationType);
                }
                else
                {
                    var _applicationType = _applicationDbContext.ApplcationTypes.SingleOrDefault(value => value.ClientId == model.ClientId
                                                                                                && value.ClientSecret == model.ClientSecret);
                    return await GenereteToken(appUser, _applicationType);
                }
            }
            else
            {
                return BadRequest("Invalid login attempt.");
            }
        }

        private async Task<object> GenereteToken(ApplicationUser appUser, ApplicationType type)
        {
            var userRole = await _userManager.GetRolesAsync(appUser);

            return ReturnJWT(appUser, userRole, type);
        }
        private async Task<object> GenereteTokenRefresh(RefreshToken refreshTokenFrom)
        {
            var userRole = await _userManager.GetRolesAsync(refreshTokenFrom.Identity);

            return ReturnJWT(refreshTokenFrom.Identity, userRole, refreshTokenFrom.ApplicationType);
        }

        private async Task<object> ReturnJWT(ApplicationUser appUser, IList<string> userRole, ApplicationType type)
        {
            var token = await new CreateToken().GenerateJwtToken(appUser, _userManager);
            _applicationDbContext.RefreshTokens.Add(
            new RefreshToken()
            {
                Token = token.Item2,
                Expiration = false,
                ApplicationType = type,
                Identity = appUser
            });
            await _applicationDbContext.SaveChangesAsync();

            return new
            {
                appUser.UserName,
                token.Item1,
                token.Item2,
                userRole
            };
        }

        [AllowAnonymous]
        [HttpPost("LoginRefresh")]
        public async Task<object> Login([FromBody] LoginTokenViewModel model) //login with JWT
        {
            var refreshTokenFrom = _applicationDbContext.RefreshTokens.
                Include(value => value.Identity).
                Include(value => value.ApplicationType).
                SingleOrDefault(item => item.Token == model.Token);

            if (refreshTokenFrom is null)
            {
                return BadRequest();
            }
            if(refreshTokenFrom.Identity.IsEnabled.GetValueOrDefault())
            {
                refreshTokenFrom.Expiration = true;
                return BadRequest("User is locked. Please reset to password.");
            }
            if (refreshTokenFrom.Expiration)
            {
                return BadRequest("RefreshToken is expired");
            }

            refreshTokenFrom.Expiration = true;

            refreshTokenFrom.Identity.LastVisit = DateTimeOffset.Now;

            _applicationDbContext.Update(refreshTokenFrom);
            await _applicationDbContext.SaveChangesAsync();

            await _signInManager.SignInAsync(refreshTokenFrom.Identity, false, null);

            return await GenereteTokenRefresh(refreshTokenFrom);
        }



        [HttpGet]
        [AllowAnonymous]
        public async Task<IActionResult> ConfirmEmail(string userId, string code)
        {
            var user = await _userManager.FindByIdAsync(userId);
            if (user == null)
            {
                throw new ApplicationException($"Unable to load user with ID '{userId}'.");
            }
            var result = await _userManager.ConfirmEmailAsync(user, code);
            if (result.Succeeded)
            {
                user.EmailConfirmed = true;
                _applicationDbContext.Update(user);
                await _applicationDbContext.SaveChangesAsync();

                return RedirectToAction(nameof(EmailConfirmation));
            }
            return BadRequest(result.Errors);
        }
        [HttpGet("ResetPassword")]
        [AllowAnonymous]
        public IActionResult ResetPassword(string email = null, string code = null)
        {
            if (code == null)
            {
                throw new ApplicationException("A code must be supplied for password reset.");
            }
            var model = new ResetPasswordViewModel { Email = email, Code = code };
            return View(model);
        }


        [HttpPost("ResetPassword")]
        [AllowAnonymous]
        public async Task<IActionResult> ResetPassword([FromForm] ResetPasswordViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }
            ApplicationUser appUser = await _userManager.FindByEmailAsync(model.Email);
            if (appUser == null)
            {
                // Don't reveal that the user does not exist
                return RedirectToAction(nameof(ResetPasswordConfirmation));
            }
            if (appUser.IsEnabled.GetValueOrDefault())
                appUser.IsEnabled = false;
            var result = await _userManager.ResetPasswordAsync(appUser, model.Code, model.Password);
            if (result.Succeeded)
            {
                return RedirectToAction(nameof(ResetPasswordConfirmation));
            }
            return View();
        }


        [HttpGet("ResetPasswordConfirmation")]
        [AllowAnonymous]
        public IActionResult ResetPasswordConfirmation()
        {
            return View();
        }
        [HttpGet("EmailConfirmation")]
        [AllowAnonymous]
        public IActionResult EmailConfirmation()
        {
            return View();
        }

        [HttpPost("ForgotPassword")]
        [AllowAnonymous]
        public async Task<IActionResult> ForgotPassword([FromBody] ForgotPasswordViewModel model)
        {
            if (ModelState.IsValid)
            {
                var user = await _userManager.FindByEmailAsync(model.Email);
                if (user == null || !(await _userManager.IsEmailConfirmedAsync(user)))
                {
                    //TODO make message
                    // Don't reveal that the user does not exist or is not confirmed
                    return BadRequest("");
                }

                var code = await _userManager.GeneratePasswordResetTokenAsync(user);
                var callbackUrl = Url.ResetPasswordCallbackLink(user.Email, code, Request.Scheme);
                await _emailSender.SendEmailAsync(model.Email, "Reset Password",
                   $"Please reset your password by clicking here: <a href='{callbackUrl}'>link</a>");
            }
            return Ok("Please check your email to reset your password.");

        }
    }
}