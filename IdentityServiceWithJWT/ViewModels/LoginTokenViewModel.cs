﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace IdentityServiceWithJWT.ViewModels
{
    public class LoginTokenViewModel
    {
        public Guid Token { set; get; }
    }
}
