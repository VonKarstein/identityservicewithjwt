﻿using IdentityServiceWithJWT.Models.ApplicationInformation;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace IdentityServiceWithJWT.ViewModels
{
    public class LoginViewModel
    {
        //[Required]
        //[EmailAddress]
        public string Email { get; set; }

        //[Required]
        //[DataType(DataType.Password)]
        public string Password { get; set; }

        public string ClientId { set; get; }
        public string ClientSecret { set; get; }

        //[Display(Name = "Remember me?")]
        //public bool RememberMe { get; set; }
    }
}
